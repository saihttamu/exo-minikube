from django.db import models

# Create your models here.

class Film(models.Model):
    title = models.TextField()
    year = models.PositiveSmallIntegerField()