# exo-minikube

Objectif : Déployer une API Restful sur un cluster Kubernetes que vous aurez préalablement mis en place.


## Infos

Pour l'instant l'exercice n'est pas finalisé. L'api est viable. On peut la tester avec une base de données postgres en local ou en déployant la base dans un cluster kubernetes.

Voici une première procédure pour lancer l'api


## Installation

Installez les éléments suivants :
  - [minikube](https://kubernetes.io/fr/docs/tasks/tools/install-minikube/) (assurez-vous d'avoir [docker]() d'installé également)
  - [python](https://www.python.org/downloads/)
  - [postgresql](https://www.postgresql.org/download/) et pgadmin si vous souhaitez tester l'api avec la bdd en local

Clonez le répo et placez-vous dans la racine du projet puis lancez la commande suivante :

```python
pip install -r pip-requirements.txt
```


## Lancement du cluster

Lancez les commandes suivantes pour lancer le cluster et déployer la base de données qui servira à l'api:

```cmd
minikube start

minikube kubectl -- apply -f ./minikube/postgres/postgres-deploy.yml
minikube kubectl -- apply -f ./minikube/postgres/postgres-config.yml
minikube kubectl -- apply -f ./minikube/postgres/postgres-pv-pvc.yml
minikube kubectl -- apply -f ./minikube/postgres/postgres-service.yml

minikube tunnel
```


## Lancement de l'api

Toujours dans le dossier racine du projet, lancez les commandes suivantes pour établir le lien à la BDD:

```cmd
python ./api/manage.py makemigrations
python ./api/manage.py migrate
```

Une fois les migrations faites nous pouvons lancer l'api :
```cmd
python ./api/manage.py runserver
```

Vouys devriez accéder à l'api via l'url suivante : http://127.0.0.1:8000/api/films

Vous pouvez ajouter des éléments à la BDD


## Avec BDD en local

Si l'on souhaite connecter l'api avec une autre BDD il faut modifier la configuration dans ./api/api/settings.py pour la faire correpondre avec celle voulue.



```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'minikube',
        'USER': 'postgres',
        'PASSWORD': 'Mdp123',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}
```

Ne pas modifier le champ 'ENGINE' pour une BDD postgresql




## Prochaines étapes

- [ ] ajout d'un Dockerfile  pour build l'api dans une image Docker
- [ ] déployer l'api dans le cluster
- [ ] configurer gitlab pour déployer vers le cluster
